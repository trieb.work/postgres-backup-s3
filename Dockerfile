FROM node:14-alpine

# Create app directory
WORKDIR /usr/src/app

RUN apk update && apk add --no-cache "postgresql14-client>14"
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# If you are building your code for production
RUN npm ci --only=production

# Bundle app source
COPY . .

CMD [ "node", "db_backup.js" ]