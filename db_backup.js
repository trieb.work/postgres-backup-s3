// call this script with the following environment variables:
// S3_KEY           //   DATABASES
// S3_SECRET        //   NUMBER_OF_VERSIONS  
// S3_ENDPOINT  // S3_BUCKET_NAME
// POSTGRES_DB_HOST   // POSTGRES_DB_PORT
//  POSTGRES_DB_USER  // POSTGRES_DB_PASSWORD
const moment = require('moment')
const AWS = require('aws-sdk');
const fs = require('fs')


const { S3_KEY, S3_SECRET, S3_ENDPOINT, S3_BUCKET_NAME, S3_REGION,  POSTGRES_DB_HOST, POSTGRES_DB_PORT, POSTGRES_DB_USER, POSTGRES_DB_PASSWORD, DATABASES, NUMBER_OF_VERSIONS } = process.env;

let s3Region = S3_REGION;
if (!S3_ENDPOINT) throw new Error("S3 Endpoint missing. Set it via S3_ENDPOINT")
if (!S3_KEY) throw new Error("S3 key missing. Set it via S3_KEY")
if (!S3_SECRET) throw new Error("S3 Secret missing. Set via S3_SECRET")
if (!S3_BUCKET_NAME) throw new Error("S3 bucket name missing. Set via S3_BUCKET_NAME")
if (!s3Region) s3Region = "auto"
const spacesEndpoint = new AWS.Endpoint(S3_ENDPOINT);
const spaces = new AWS.S3({
    endpoint: spacesEndpoint,
    accessKeyId: S3_KEY,
    secretAccessKey: S3_SECRET,
    region: s3Region,
    s3ForcePathStyle: true
});


async function run() {
    try {
        let databaseArray
        if (DATABASES) {
            databaseArray = DATABASES.split(',')
        } else {
            throw new Error('no databases defined')
        }
        var params = {
            Bucket: S3_BUCKET_NAME,
            ACL: "private"
        };
        // create the bucket
        try {
            await spaces.createBucket(params).promise()
        } catch (error) {
            if (error.statusCode === 409) console.log('Bucket already exist, continue')

        }


        for (database of databaseArray) {
            console.log('Dumping now', database)
            const filename = `${moment().format('YYYY-MM-DD_HHmm')}.${database}.dump.sql`

            await execShellCommand(`PGPASSWORD=${POSTGRES_DB_PASSWORD} pg_dump -O -h ${POSTGRES_DB_HOST} -p ${POSTGRES_DB_PORT} -U ${POSTGRES_DB_USER} ${database} > ${filename}`)

            const rs = fs.createReadStream('./' + filename)

            params.Body = rs
            params.ACL = 'private'
            params.Key = filename

            // upload the file
            let result = await spaces.upload(params).promise()
            console.log('Pushed the following files:', result)

            delete params.Body
            delete params.ACL
            delete params.Key
        }

        // cleanup Job
        let files = await spaces.listObjectsV2(params).promise()
        files = files.Contents
        files = files.map(file => {
            file.Time = moment(file.Key.slice(0, 15), 'YYYY-MM-DD_HHmm').format()
            return file
        })
        // sort by time
        files = files.sort((a, b) => b.Time - a.Time)
        if (Math.sign(files.length - NUMBER_OF_VERSIONS) > 0) {
            const AmountToDelete = files.length - NUMBER_OF_VERSIONS
            let toDelete = files.slice(0, AmountToDelete)
            console.log('We are deleting the oldest', AmountToDelete, 'version(s)')

            for (file of toDelete) {
                console.log('file', file)
                params.Key = file.Key
                let result = await spaces.deleteObject(params).promise()

            }


        }




    } catch (error) {
        console.log('Error', error)
        process.exit(1)

    }






}

run()


function execShellCommand(cmd) {
    const exec = require('child_process').exec;
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.warn(error);
            }
            resolve(stdout ? stdout : stderr);
        });
    });
}
